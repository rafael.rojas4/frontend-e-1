let button = document.getElementById("submit");
let valid;

document.getElementById('form').onsubmit = (event) => {
	event.preventDefault();
	valid = true;

	validateArrivalDate();
	validateNights();
	validateName();
	validatePhone();
	validateEmail();
	
	if (valid) {
		storeForm();
	}
};

document.getElementById('form').onreset = (event) => {
	resetErrorMessageValues();
};

let validateArrivalDate = () => {
	let arrivalDate = document.getElementById("arrivalDate");
	let errorMessage = document.getElementById('arrivalDateErrorMessage');
	
	if(!arrivalDate.value) {
		errorMessage.innerText = 'Arrival date is required';
		valid = false;
		return;
	}

	let dateInformation = new Date(arrivalDate.value.replace("/","-"));
	let actualDate = new Date();
	if (actualDate > dateInformation) {
		errorMessage.innerText = 'Must be greater than actual date';
		valid = false;
		return;
	}

	errorMessage.innerText = '';
}

let validateNights = () => {
	let nightDate = document.getElementById("nights");
	let valid = true;

	let errorMessage = document.getElementById('nightsErrorMessage');
	
	if(!nightDate.value) {
		errorMessage.innerText = 'Number of nights is required';
		valid = false;
		return;
	}

	let numberFormat = /^[0-9]*$/

	if (!nightDate.value.match(numberFormat)) {
		errorMessage.innerText = 'Nights must be a number';
		valid = false;
		return;
	}

	errorMessage.innerText = '';
	return valid;
}

function validateName() {
	const nameField = document.getElementById("name");
	let errorMessage = document.getElementById('nameErrorMessage');
  
	if(!nameField.value){
		errorMessage.innerText = 'Name is required';
		valid = false;
		return;
	}

	errorMessage.innerText = '';
}

function validateEmail() {
	const emailField = document.getElementById("email");
	let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	let errorMessage = document.getElementById('emailErrorMessage')
	if(!emailField.value){
		errorMessage.innerText = 'Email is required';
		valid = false;
		return;
	}
	if (!emailField.value.match(mailformat)){
		errorMessage.innerText = 'Email must be valid!';
		valid = false;
		return;
	}

	errorMessage.innerText = '';
	return valid;
}

function validatePhone() {
	const numberField = document.getElementById("phone");
	let errorMessage = document.getElementById('phoneErrorMessage');
	
	if(!numberField.value){
		errorMessage.innerText = 'Phone is required';
		valid = false;
		return;	
	}

	let phoneformat = /(7|8|9)\d{8}/;
	if (!numberField.value.match(phoneformat)){
		errorMessage.innerText = 'Phone must be valid!';
		valid = false;
		return;
	}

	errorMessage.innerText = '';
}

function storeForm(){
	let arrivalDate = document.getElementById("arrivalDate").value;
	let nightDate = document.getElementById("nights").value;
	let adults = document.getElementById("adults").value;
	let children = document.getElementById("children").value;
	let roomType;
	document.getElementsByName('room').forEach((element) => {
		if(element.checked) {
			roomType = element.name;
		}
	});
	let bedType;
	document.getElementsByName('bed').forEach((element) => {
		if(element.checked) {
			bedType = element.name;
		}
	});
	let smoking = document.getElementById("smoking").value;
	let name = document.getElementById("name").value;
    let email = document.getElementById("email").value;
	let phone = document.getElementById("phone").value;
    localStorage["arrivalDate"] = arrivalDate;
    localStorage["nightDate"] = nightDate;
	localStorage["adults"] = adults;
    localStorage["children"] = children;
	localStorage["roomType"] = adults;
    localStorage["bedType"] = bedType;
	localStorage["smoking"] = smoking;
	localStorage["name"] = name;
    localStorage["email"] = email;
	localStorage["phone"] = phone;
    console.log(localStorage);

}

let resetErrorMessageValues = () => {
	document.getElementById('phoneErrorMessage').innerText = '';
	document.getElementById('nightsErrorMessage').innerText = '';
	document.getElementById('emailErrorMessage').innerText = '';
	document.getElementById('nameErrorMessage').innerText = '';
	document.getElementById('arrivalDateErrorMessage').innerText = '';
}